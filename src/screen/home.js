import React, { Component } from 'react';
import {Image, View, StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native';
import Expo, { Font } from "expo";

import Swiper from '../homescreen/swiper';
import Newsinfo from '../homescreen/newsinfo';
import Kuliner from '../homescreen/kuliner';
import Jadwal from '../homescreen/jadwal';

import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Body, Title, Thumbnail, Input, Item, List, ListItem } from 'native-base';
import { AppHeader, AppFooter } from '../app-nav/index';

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show : true,
      news : []
    }
  }

  componentDidMount() {
    AsyncStorage.setItem("Splash", 'yes');
  }

  render() {
    return (
      <Container>
        <AppHeader isHome isMenu />
        <Header searchBar rounded style={{ backgroundColor: 'white' }}>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari" onChangeText={this.handleSearch} />
          </Item>
        </Header>
        <Content style={{}}>

          {this.state.show ?
            <View>
              <Swiper />
              <View style={styles.infoBerita}>
                <Text style={styles.infoText}>Info Terkini</Text>
              </View>
              <View style={styles.panelBerita}>
                <Newsinfo navigation={this.props.navigation} />
              </View>
              <View style={styles.infoBerita}>
                <Text style={styles.infoText}>Rekomendasi Kuliner Di Sekitar Anda</Text>
              </View>
              <View style={styles.panelBerita}>
                <Kuliner navigation={this.props.navigation} />
              </View>
              <View style={styles.infoSholat}>
                <Text style={styles.infoText}>Jadwal Sholat</Text>
              </View>
              <View style={styles.panelBerita}>
                <Jadwal />
              </View>
            </View> :
            <List>
            {this.state.news.length != 0 ?
              this.state.news.map((item, idx) => {
              return (
                <View key={idx}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("NewsDetail", { idBerita : item._id })}>
                  <View style={{flex: 1, backgroundColor: "#ffffff", flexDirection: 'row',borderColor: '#f8f8f8', borderBottomWidth: 2}}>
                    <Thumbnail square source={{uri: item.newsImage}} style={styles.imageBeritaSearch} />
                    <Body style={styles.wordBerita}>
                      <Text style={styles.wordTitle}>{sliceNewsTitle(item.newsTitle)}</Text>
                      <Text style={styles.wordDate}>{item.newsStartDate}</Text>
                      <Text style={styles.wordDesc}>{sliceNewsDescription(item.newsDescription)}</Text>
                    </Body>
                  </View>
                </TouchableOpacity>
                </View>
              )
            }):
            <View>
              <Text style={{alignItems:"center",alignSelf:"center", marginTop:"5%"}}>Data Tidak Ada</Text>
              </View>}
          </List>
          }
        </Content>
        {/*<AppFooter navigation={this.props.navigation} />*/}
      </Container>
    );
  }

  handleSearch = (text) => {
    if (text === "" ||text === " " || text.length == 0 || text == null) {
          this.setState({ show: true });
    } else {
      fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_search/" + text, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then((data) => {
          this.setState({ news: data });
          this.setState({ show: false });
        })
    }
  }
}

function sliceNewsTitle(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

function sliceNewsDescription(text) {
  return text.length > 60 ? text.substring(0, 60) + "..." : text;
}


const styles = StyleSheet.create({
  infoBerita: {
    paddingTop: "5%",
    alignItems: "center",
    backgroundColor: "#ffffff",
    paddingBottom: "5%",
  },
  infoSholat: {
    alignItems: "center",
    paddingLeft: "5%",
    paddingTop: "10%", 
    backgroundColor: "#ffffff",
    paddingBottom: "5%",
  },
  infoText: {
    fontFamily: 'Ubuntu_Light',
  },
  imageBerita: {
    borderRadius: 3,
    marginLeft: "2%",
    marginTop: "2%",
    width: "33%",
    height: "10%"
  },
  panelBerita: {
    backgroundColor: "#f8f8f8",
    padding: "3%",
  },
   imageBeritaSearch: {
      borderRadius:3,
      margin:"3%",
      width: 100,
      height: 70,
    },
    wordBerita: {
      alignItems: 'flex-start',
      margin:"2%",
    },
    wordDate: {
      fontFamily:'Ubuntu_Light',
      fontSize: 10,
      color:"#363636",
    },
    wordTitle: {
      fontFamily: 'Ubuntu_Regular',
      fontSize: 14,
    },
    wordDesc: {
      marginTop:'2%',
      fontFamily:'Ubuntu_Light',
      fontSize: 12,
    },
});