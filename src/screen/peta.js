import Expo from 'expo';
import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet, AsyncStorage } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
import MapView from 'react-native-maps';
import { Container, Content } from 'native-base';
import { AppHeader, AppFooter } from "../app-nav/index";

const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };

export default class Peta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jsonData: '',
            marker: this.props.navigation.state.params.location
        }
    }

    state = {
        location: { coords: { latitude: 0, longitude: 0 } },
    };

    componentWillMount() {

        Location.watchPositionAsync(GEOLOCATION_OPTIONS, this.locationChanged);
    }

    componentDidMount() {

        navigator.geolocation.getCurrentPosition((position) => {
            this.setState({ position: { longitude: position.longitude, latitude: position.latitude } });
        }, (error) => {
            alert(JSON.stringify(error))
        }, {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000
        });       
    }


    locationChanged = (location) => {
        region = {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.1,
            longitudeDelta: 0.05,
        },
            this.setState({ location, region })
    }

    render() {
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title="Point of Interest" />
                <Content>
                </Content>
                <View style={styles.container}>
                    <MapView
                        style={styles.map}
                        showsUserLocation={true}
                        region={this.state.region}
                        onRegionChange={this.onRegionChange}
                    >
                        <MapView.Marker
                            coordinate={{
                                latitude: this.state.marker.geometry.location.lat,
                                longitude: this.state.marker.geometry.location.lng
                            }}
                            title={this.state.marker.name}
                            description={this.state.marker.location}
                        />
                    </MapView>
                </View>
                {/*<AppFooter navigation={this.props.navigation} />*/}
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    baseText: {
        fontFamily: 'Ubuntu-Light',

    },
    titleText: {
        fontFamily: 'Ubuntu-Regular',
        fontSize: 30,
        fontWeight: 'bold',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        backgroundColor: "#fff"
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 40

    },
    container: {
        backgroundColor: "#FFF",
        position: 'absolute',
        top: 80,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
});